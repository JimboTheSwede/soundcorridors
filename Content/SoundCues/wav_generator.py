#!/usr/bin/python
# -*- coding: utf-8 -*-

# Experiment ALEXIS #107 -------------------------------------------------------
# ALEXIS #107: gamification of a psychophysical task. This script generates 
# wav-files with different ILDs, to be used by the game engine.
# MN 2016-04-05 JA 2016-04-11

import numpy as np
import scipy.io.wavfile  # For saving wav-files as checks

#Creating text file to be used as a data table for Unreal also write headers for
#columns
#Format: Name,ILD,Path
DataTable = open('ExperimentDataTable.txt', 'w+')
DataTable.write('Name,ILD,Path\n') 



def create_ild(mono, ild):
    '''Creates a dichotic signal of a monosignal, by amplifying the level in the
    left channel with half the ild and attenuating the level in the right 
    channel with half the ild. Positive ilds favor the left ear, negative ilds 
    favor the right ear (e.g., if ild = -10, then left ear = +-5 dB and
    right ear = --5 dB). Note: Maker sure that max(monosignal) < -(abs(ild)/2), 
    to avoid clipping.
    
    Keyword arguments:
    mono -- 1xn numpy array
    idl -- ild in dB (float)
    
    Return:
    dichotic -- 2xn numpy array
    '''
    lleft = ild/2
    lright = lleft * -1
    gain_left = 10**((lleft)/20.0) # gain factor to achive half-ild
    gain_right = 10**((lright)/20.0) # gain factor to achive half-ild
    sleft = mono * gain_left
    sright = mono * gain_right
    stereo = np.array([sleft,sright])
    stereo = stereo.transpose()
    return stereo


def set_gain(mono, gaindb):
    ''' Set gain of mono signal, to get db(peak) to specified gaindb 
    
    Keyword arguments:
    mono -- vector (numpy array).
    gaindb -- gain of mono in dB (float) re max = 0 dB.
    
    Return:
    gained -- numpy vector with level-adjusted mono signal
    '''
    peak = np.max(mono)
    adjust = gaindb - 20 * np.log10(peak)
    gained = 10**(adjust/20.0) * mono # don't forget to make 20 a float (20.0)
    
    # Print warning if overload, that is, if any abs(sample-value) > 1
    if (np.max(np.abs(gained)) > 1):
        message1 = "WARNING: set_gain() generated overloaded signal!"
        message2 = "max(abs(signal)) = " + str(np.max(np.abs(gained))) 
        message3 = ("number of samples >1 = " + 
                    str(np.sum(1 * (np.abs(gained) > 1))))
        print message1
        print message2
        print message3
 
    return gained

    
def create_square_pulse(samples):
    ''' Create square pulse, startig and ending with zeros of 10 x length as the
    plateau of ones.
    
    Keyword arguments:
    samples -- length of plateau (integer). NB! length numpy array = 3 x samples
    
    Return:
    squarew = numpy array with square wave
    '''
    
    square_list = [0]*(10*samples) + [1]*samples + [0]*(10*samples)
    square_array = np.array(square_list)
    return square_array


# Set these parameters----------------------------------------------------------    
fs = 48000

# Level of mono signal used to create ilds. To avoid clips, 
# set dbgain to less than -abs(ild)/2
dbgain = -11 

# num = 401 to get 0.1 ild-steps between -20 and 20
num = 401 
ilds = np.linspace(-20, 20, num)

# Creaet mono sound (square wave, other wave form may be used)
mono = create_square_pulse(10) # 10 length in samples of square plateau
#-------------------------------------------------------------------------------


# Create and save wav-files-----------------------------------------------------
# Set gain of mono signal = gain of dichotic with ild = 0 dB
mono = set_gain(mono, dbgain)

# For naming files: they are namned in "centibels", that is, 1.wav = 0.1 dB, 
# -5.wav = -0.5 dB, 20.wav = 2 dB, 200.wav = 20 dB, -15.wav = -1.5 dB, ...
ild_name = np.round(ilds*10) 
counter = 0 
# This loop creates the wavfiles
for j in range(len(ilds)):
    
    # Create numpy array representing sound
    signal = create_ild(mono, ilds[j])
    print "max(signal)", np.amax(signal)
    
    # Create wav-file name, and save wav-file
    a =  str(int(ild_name[j]))
    wavname = a + '.wav'
    scipy.io.wavfile.write(wavname, fs, signal)
	#Writing to DataTable
    counter += 1
    b = str(counter)
    DataTable.write(b)
    DataTable.write(',')
    DataTable.write(a)
    DataTable.write(',')
    DataTable.write("\"SoundWave\'/Game/SoundCues/SoundFiles/")
    DataTable.write(a)
    DataTable.write(".")
    DataTable.write(a)
    DataTable.write("\'\"")
    DataTable.write("\n")
    
    
    
    
DataTable.close()
	
	
#-------------------------------------------------------------------------------
